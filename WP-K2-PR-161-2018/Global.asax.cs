﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WP_K2_PR_161_2018.Models;

namespace WP_K2_PR_161_2018
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            List<User> users = DataLists.ReadUsers("~/App_Data/RegisteredUsers.txt");
            HttpContext.Current.Application["Users"] = users;

            List<Proizvod> proizvods = DataLists.ReadProducts("~/App_Data/Products.txt");
            HttpContext.Current.Application["Proizvodi"] = proizvods;

            List<Kupovina> kup = new List<Kupovina>();
            HttpContext.Current.Application["Kupovina"] = kup;
        }
    }
}
