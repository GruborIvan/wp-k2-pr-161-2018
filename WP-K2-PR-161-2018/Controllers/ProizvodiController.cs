﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WP_K2_PR_161_2018.Models;

namespace WP_K2_PR_161_2018.Controllers
{
    public class ProizvodiController : Controller
    {

        // GET: Proizvodi
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddNewProduct()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ModifyProizvod(int id)
        {
            Session["ModifProizv"] = null;
            
            Proizvod p;
            List<Proizvod> products = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            foreach (Proizvod pp in products.ToList())
            {
                if (pp.id == id)
                {
                    p = pp;
                    Session["ModifProizv"] = p;
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult replaceProduct(Proizvod proiz)
        {
            Session["Error"] = null;
            if (proiz.Naziv == null || proiz.AdresaProizvodjaca == null || proiz.Boja == null || proiz.VrstaMeda == null || proiz.Proizvodjac == null || proiz.CenaPoTegli == 0)
            {
                Session["Error"] = "Please fill out all the required fields!";
                return View("ModifyProizvod");
            }

            if (proiz.Naziv.Length < 3)
            {
                Session["Error"] = "Product name too short!";
                return View("ModifyProizvod");
            }

            List<Proizvod> products = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            foreach (Proizvod pr in products.ToList())
            {
                if (pr.id == proiz.id)
                {
                    products.Add(proiz);
                    products.Remove(pr);            
                }
            }
            DataLists.PregaziFajl();
            ViewBag.Producty = products;
            return View("~/Views/Home/Index.cshtml");
        }
        
        [HttpPost]
        public ActionResult AddThisProduct(Proizvod proiz)
        {
            Session["ErrorProduct"] = null;
            if (proiz.AdresaProizvodjaca == null || proiz.Boja == null || proiz.CenaPoTegli == 0 || proiz.Naziv == null || proiz.Opis == null || proiz.Proizvodjac == null || proiz.TeglasLeft == 0 || proiz.VrstaMeda == null)
            {
                Session["ErrorProduct"] = "Please fill all required fields!";
                return View("~/Views/Proizvodi/AddNewProduct.cshtml");
            }

            List<Proizvod> products = (List<Proizvod>)HttpContext.Application["Proizvodi"];

            int max = -1;
            foreach(Proizvod pr in products.ToList())
            {
                if (pr.id > max)
                    max = pr.id;
            }
            max++;
            proiz.id = max;
            products.Add(proiz);
            DataLists.WriteProducts(proiz);
            ViewBag.Producty = products;
            return View("~/Views/Home/Index.cshtml");
        }

        [HttpPost]
        public ActionResult FilterCena(int od, int doo)
        {
            List<Proizvod> products = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            List<Proizvod> ret = new List<Proizvod>();
            foreach(Proizvod pr in products)
            {
                if ((pr.CenaPoTegli >= od) && (pr.CenaPoTegli <= doo))
                {
                    ret.Add(pr);
                }
            }
            ViewBag.Producty = ret;
            return View("~/Views/Home/Index.cshtml");
        }

        [HttpPost]
        public ActionResult Reset()
        {
            List<Proizvod> ret = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            ViewBag.Producty = ret;
            return View("~/Views/Home/Index.cshtml");
        }

        [HttpPost]
        public ActionResult FindByVrsta(string vrsta)
        {
            List<Proizvod> proiz = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            List<Proizvod> ret = new List<Proizvod>();
            foreach(Proizvod pr in proiz)
            {
                if (pr.VrstaMeda.Contains(vrsta))
                    ret.Add(pr);
            }
            ViewBag.Producty = ret;
            return View("~/Views/Home/Index.cshtml");
        }

        [HttpPost]
        public ActionResult FindByNaziv(string naziv)
        {
            List<Proizvod> proiz = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            List<Proizvod> ret = new List<Proizvod>();
            foreach (Proizvod pr in proiz)
            {
                if (pr.Naziv.Contains(naziv))
                    ret.Add(pr);
            }
            ViewBag.Producty = ret;
            return View("~/Views/Home/Index.cshtml");
        }

        [HttpPost]
        public ActionResult Sort(string chosen,string updown)
        {
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            
            if (chosen.Equals("NAZIV"))
            {
                var sortedQuery = proizvodi.OrderBy(Proizvod => Proizvod.Naziv);
                proizvodi = sortedQuery.ToList();
            }
            else if (chosen.Equals("VRSTA MEDA"))
            {
                var sortedQuery = proizvodi.OrderBy(Proizvod => Proizvod.VrstaMeda);
                proizvodi = sortedQuery.ToList();
            }
            else if (chosen.Equals("CENA"))
            {
                var sortedQuery = proizvodi.OrderBy(Proizvod => Proizvod.CenaPoTegli);
                proizvodi = sortedQuery.ToList();
            }
            else
            {
                // Nista se ne desi ukoliko se prepozna nesto sto program ne prepoznaje.
            }

            if (updown.Equals("OPADAJUĆE"))
            {
                List<Proizvod> proizvodi2 = new List<Proizvod>();
                for (int i = (proizvodi.Count - 1); i >= 0; i--)
                    proizvodi2.Add(proizvodi[i]);
                proizvodi = proizvodi2;
            }
            
            ViewBag.Producty = proizvodi;
            return View("~/Views/Home/Index.cshtml");
        }

    }
}