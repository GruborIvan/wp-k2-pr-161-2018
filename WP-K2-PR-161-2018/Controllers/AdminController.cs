﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WP_K2_PR_161_2018.Models;

namespace WP_K2_PR_161_2018.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult KupacList()
        {
            List<User> users = (List<User>)HttpContext.Application["Users"];
            List<User> kupci = new List<User>();

            foreach (User us in users)
            {
                if (us.Type.Equals(UserType.KUPAC))
                {
                    kupci.Add(us);
                }
            }
            ViewBag.kupci = kupci;
            return View();
        }

        public ActionResult DeleteUser(string username)
        {
            List<User> users = (List<User>)HttpContext.Application["Users"];
            List<User> usrss = users.ToList();

            foreach (User u in usrss.ToList())
            {
                if (u.Type.Equals(UserType.ADMINISTRATOR))
                    usrss.Remove(u);
                if (u.Username.Equals(username))
                {
                    users.Remove(u);
                    usrss.Remove(u);
                }
                    
            }
            ViewBag.kupci = usrss;
            return View("~/Views/Admin/KupacList.cshtml");
        }


    }
}