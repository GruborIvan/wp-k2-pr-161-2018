﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WP_K2_PR_161_2018.Models;

namespace WP_K2_PR_161_2018.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Log()
        {
            User u = (User)Session["User"];
            if (u == null)
            {
                return View("~/Views/Autentifikacija/Index.cshtml");
            }
            else
            {
                return View("~/Views/Home/Index.cshtml");
            }
        }

        public ActionResult Index()
        {
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            foreach (Proizvod pr in proizvodi.ToList())
            {
                if (pr.TeglasLeft == 0)
                    proizvodi.Remove(pr);
            }
            ViewBag.Producty = proizvodi;
            return View();
        }

    }
}