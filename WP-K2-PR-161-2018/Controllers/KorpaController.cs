﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WP_K2_PR_161_2018.Models;

namespace WP_K2_PR_161_2018.Controllers
{
    public class KorpaController : Controller
    {
        // GET: Korpa
        public ActionResult KorpaView()
        {
           User current = (User)Session["User"];
           List<Kupovina> kupovine = (List<Kupovina>)HttpContext.Application["Kupovina"];
           List<Kupovina> kupovinePriv = new List<Kupovina>();
           foreach (Kupovina kup in kupovine.ToList())
           {
               if (kup.kupac.Username == current.Username)
               {
                    if (kup.proizvod.TeglasLeft != 0)
                    {
                        kupovinePriv.Add(kup);
                    }       
               }
           }
           if (kupovinePriv.Count == 0)
           {
                Session["Empty"] = "False";
           }
           else
           {
                ViewBag.Kup = kupovinePriv;
                Session["Empty"] = "True";
           }

           return View();
        }

        [HttpPost]
        public ActionResult Kupi(int id)
        {
            List<Proizvod> proizvods = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            Proizvod kp;
            foreach(var kup in proizvods.ToList())
            {
                if (kup.id == id)
                {
                    kp = kup;
                    ViewBag.Chosen = kp;
                }
            }
            return View("BuyDetails");
        }

        [HttpPost]
        public ActionResult KupiProizvod(Proizvod pr, int kolicina)
        {
            if (pr.TeglasLeft < kolicina)
            {
                Session["ErrorKol"] = "Nema na raspolaganju zahtevani broj tegla meda";
                ViewBag.Chosen = pr;
                return View("BuyDetails");
            }

            List<Proizvod> proiz = (List<Proizvod>)HttpContext.Application["Proizvodi"];
            foreach (var pro in proiz.ToList())
            {
                if (pro.id == pr.id)
                    pro.TeglasLeft -= kolicina;
            }
            
            User u = (User)Session["User"];
            Kupovina nova = new Kupovina(u, pr, DateTime.Now, kolicina, pr.CenaPoTegli * kolicina);
            List<Kupovina> kupovine = (List<Kupovina>)HttpContext.Application["Kupovina"];
            kupovine.Add(nova);
            return RedirectToAction("KorpaView","Korpa");
        }

    }
}