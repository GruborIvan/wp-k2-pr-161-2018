﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WP_K2_PR_161_2018.Models;

namespace WP_K2_PR_161_2018.Controllers
{
    public class AutentifikacijaController : Controller
    {
        [HttpPost]
        public ActionResult LogIn(string username,string password)
        {
            Session["Error"] = null;
            List<User> users = (List<User>)HttpContext.Application["Users"];
            bool logged = false;
            foreach (User u in users)
            {
                if (u.Username == username && u.Password == password)
                {
                    Session["User"] = u;
                    u.LoggedIn = true;
                    logged = true;
                    return RedirectToAction("Index", "Home");
                }
            }
            Session["Error"] = "Invalid username/password combination. Try again!";
            return View("~/Views/Autentifikacija/Index.cshtml");
        }


        public ActionResult LogOut()
        {
            User u = (User)Session["User"];
            u.LoggedIn = false;
            Session["User"] = null;
            return RedirectToAction("Log","Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegisterUser(User user)
        {
            List<User> users = (List<User>)HttpContext.Application["Users"];
            Session["Error"] = null;
            
            if (user.Username == null || user.Ime == null || user.Prezime == null || user.Password == null || user.Email == null || user.Date == null || user.Pol.Equals(""))
            {
                Session["Error"] = "Please fill out all fields";
                return View("~/Views/Autentifikacija/Register.cshtml");
            }
      
            if (user.Username.Length < 3)
            {
                Session["Error"] = "Username too short!";
                return View("~/Views/Autentifikacija/Register.cshtml");
            }

            foreach (User u in users)
            {
                if (u.Username == user.Username)
                {
                    Session["Error"] = "User with this username alredy exists. Choose a new one!";
                    return View("~/Views/Autentifikacija/Register.cshtml");
                }

            }

            user.Type = UserType.KUPAC;
            users.Add(user);
            DataLists.WriteUsers(user);
            return View("~/Views/Autentifikacija/Index.cshtml");
            
        }

    }
}