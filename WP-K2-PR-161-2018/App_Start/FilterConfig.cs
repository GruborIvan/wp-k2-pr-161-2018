﻿using System.Web;
using System.Web.Mvc;

namespace WP_K2_PR_161_2018
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
