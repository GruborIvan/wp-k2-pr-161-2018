﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WP_K2_PR_161_2018.Models
{
    public class Kupovina
    {
        public static int cnt = 0;
        public int Kupovina_ID { get; set; }
        public User kupac { get; set; }
        public Proizvod proizvod { get; set; }
        public DateTime datum { get; set; }
        public int Broj_Tegli { get; set; }
        public int Ukupna_Cena { get; set; }

        public Kupovina()
        {

        }

        public Kupovina(User kupac, Proizvod proizvod, DateTime datum, int broj_Tegli, int ukupna_Cena)
        {
            Kupovina_ID = cnt++;
            this.kupac = kupac;
            this.proizvod = proizvod;
            this.datum = datum;
            Broj_Tegli = broj_Tegli;
            Ukupna_Cena = ukupna_Cena;
        }

        public Kupovina(int kupovina_ID, User kupac, Proizvod proizvod, DateTime datum, int broj_Tegli, int ukupna_Cena)
        {
            Kupovina_ID = kupovina_ID;
            this.kupac = kupac;
            this.proizvod = proizvod;
            this.datum = datum;
            Broj_Tegli = broj_Tegli;
            Ukupna_Cena = ukupna_Cena;
        }


    }
}