﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WP_K2_PR_161_2018.Models
{
    public class DataLists
    {
        
        public static List<Proizvod> ReadProducts(string path)
        {
            List<Proizvod> prod = new List<Proizvod>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('-');
                
                Proizvod p = new Proizvod(
                    tokens[0],      // Naziv proizvoda
                    tokens[1],      // Vrsta meda
                    tokens[2],      // Proizvodjac
                    tokens[3],      // Adresa proizvodjaca
                    tokens[4],      // Boja
                    tokens[5],      // Opis
                    Convert.ToInt32(tokens[6]),
                    Convert.ToInt32(tokens[7])
                    );
                prod.Add(p);
            }
            sr.Close();
            stream.Close();

            return prod;
        }

        public static void PregaziFajl()
        {
            string path = "~/App_Data/Products.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Truncate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(stream);
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Current.Application["Proizvodi"];
            foreach(Proizvod pro in proizvodi.ToList())
            {
                sw.WriteLine(pro.Naziv + "-" + pro.VrstaMeda + "-" + pro.Proizvodjac + "-" + pro.AdresaProizvodjaca + "-" + pro.Boja + "-" + pro.Opis + "-" + pro.CenaPoTegli + "-" + pro.TeglasLeft);
            }
            sw.Close();
            stream.Close();
        }


        public static void WriteProducts(Proizvod pro)
        {
            string path = "~/App_Data/Products.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(stream);
            sw.WriteLine(pro.Naziv + "-" + pro.VrstaMeda + "-" + pro.Proizvodjac + "-" + pro.AdresaProizvodjaca + "-" + pro.Boja + "-" + pro.Opis + "-" + pro.CenaPoTegli + "-" + pro.TeglasLeft);
            sw.Close();
            stream.Close();
        }

        public static List<User> ReadUsers(string path)
        {
            List<User> users = new List<User>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('-');
                Enum.TryParse(tokens[7], out UserType typ);
                Enum.TryParse(tokens[4], out Pol myStatus);
                DateTime.TryParse(tokens[6], out DateTime dt);
                string str = dt.ToString("dd'/'MM'/'yyyy");
                DateTime.TryParse(str,out dt);
                //CultureInfo cultureInfo = new CultureInfo("");

                User p = new User(
                    tokens[0], // Username
                    tokens[1], // Password
                    tokens[2], // Name
                    tokens[3], // Lastname
                    myStatus,  // Pol korisnika
                    tokens[5],
                    dt,         //Datum
                    typ         // Tip korisnika
                    );
                users.Add(p);
            }
            sr.Close();
            stream.Close();

            return users;
        }

        public static void WriteUsers(User user)
        {
            string path = "~/App_Data/RegisteredUsers.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append,FileAccess.Write);
            StreamWriter sw = new StreamWriter(stream);
            sw.WriteLine(user.Username + "-" + user.Password + "-" + user.Ime + "-" + user.Prezime + "-" + user.Pol.ToString() + "-" + user.Email + "-" + user.Date.ToString("dd'/'MM'/'yyyy") + "-" + user.Type.ToString());
            sw.Close();
            stream.Close();
        }


    }
}