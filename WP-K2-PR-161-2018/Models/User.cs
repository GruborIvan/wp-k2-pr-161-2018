﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WP_K2_PR_161_2018.Models
{
    public class User
    {
        private string username;
        private string password;
        private string ime;
        private string prezime;
        private Pol pol;
        private string email;
        private DateTime date;
        private UserType type;

        public bool LoggedIn {get; set;}

        public User()
        {

        }

        public User(string username,string pass,string ime,string prezime, Pol pol, string email,DateTime date,UserType type)
        {
            Username = username;
            Password = pass;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Email = email;
            Date = date;
            Type = type;
        }

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public Pol Pol { get => pol; set => pol = value; }
        public string Email { get => email; set => email = value; }
        public DateTime Date { get => date; set => date = value; }
        public UserType Type { get => type; set => type = value; }
    }
}