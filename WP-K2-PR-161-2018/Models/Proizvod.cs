﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WP_K2_PR_161_2018.Models
{
    public class Proizvod
    {
        public static int id_cntr = 0;
        public int id { get; set; }
        public string Naziv { get; set; }
        public string VrstaMeda { get; set; }
        public string Proizvodjac { get; set; }
        public string AdresaProizvodjaca { get; set; }
        public string Boja { get; set; }
        public string Opis { get; set; }
        public int CenaPoTegli { get; set; }
        public int TeglasLeft { get; set; }

        public Proizvod()
        {
            id_cntr++;
        }

        public Proizvod(string naziv, string vrstaMeda, string proizvodjac, string adresaProizvodjaca, string boja, string opis, int cenaPoTegli, int teglasLeft)
        {
            this.id = id_cntr++;
            this.Naziv = naziv;
            this.VrstaMeda = vrstaMeda;
            this.Proizvodjac = proizvodjac;
            this.AdresaProizvodjaca = adresaProizvodjaca;
            this.Boja = boja;
            this.Opis = opis;
            this.CenaPoTegli = cenaPoTegli;
            this.TeglasLeft = teglasLeft;
        }

        /*
        public Proizvod(int id, string naziv, string vrstaMeda, string proizvodjac, string adresaProizvodjaca, string boja, string opis, int cenaPoTegli, int teglasLeft)
        {
            this.id = id;
            id_cntr++;
            Naziv = naziv;
            VrstaMeda = vrstaMeda;
            Proizvodjac = proizvodjac;
            AdresaProizvodjaca = adresaProizvodjaca;
            Boja = boja;
            Opis = opis;
            CenaPoTegli = cenaPoTegli;
            TeglasLeft = teglasLeft;
        }
       */
    }
}